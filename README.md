
<!-- README.md is generated from README.Rmd. Please edit that file -->

# dsRtutorials

<!-- badges: start -->

<!-- badges: end -->

The goal of the R package ‘dsRtutorials’ is to contain basic Data
Science Tutorials in R. The package is downloadable from the
correspondent Gitlab repository and the tutorials are easily rendered by
running the command learnr::run\_tutorial(“tutorial\_name”). In this way
this sets of tutorials can be easily exported to any machine and is
self-consistent. The only requirements to the user are to have a recent
version of R and R Studio installed, and to download and install the
CRAN version of the R package `learnr`. More information in the
following.

## Installation

You can install the released version of dsRtutorials from
[Gitlab](https://gitlab.com) with:

``` r
devtools::install_git("https://gitlab.com/EliMari/dsRtutorials")
```

If you do not have packge `devtools` installed on your machine, please
proceed first to install it. You can use install the CRAN version with
the usual command:

``` r
install.packages("devtools")
```

## Running a tutorial

In order to run of the tutorials contained in this package you can
proceed as follows.

1.  install package `learnr`, in case not yet available

<!-- end list -->

``` r
install.packages("learnr")
```

2.  Call library dsRtutorials and run the command to start up the
    rendered interactive tutorial.

<!-- end list -->

``` r
library(dsRtutorials)
learnr::run_tutorial("name_tutorial", package = "dsRtutorials")
```
