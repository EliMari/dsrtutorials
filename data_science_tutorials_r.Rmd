---
title: "Exercise C : Deployment of Data Science tutorials"
author: "Elisa Mariani"
output: 
  html_document:
    df_print: paged
    toc: true
    toc_float: true
    number_sections: true
params:
  renderTime: .na
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

In this document we describe briefly what are the possibilities to realise and deploy a data science tutorial using R. The described approaches are suitable for people who want to make their tutorials available for hobby as well as for professional purposes.

# Package learnr

The learnr package enables users to turn any R Markdown document into an interactive tutorial. Tutorials consist of content along with interactive components for checking and reinforcing understanding. Tutorials can include any or all of the following:

    Narrative, figures, illustrations, and equations.

    Code exercises (R code chunks that users can edit and execute directly).

    Quiz questions.

    Videos (supported services include YouTube and Vimeo).

    Interactive Shiny components.

Furthermore, tutorials are able to preserve work done within them, so if a user works on a few exercises or questions and returns to the tutorial later they can pick up right where they left off.

Tutorials are really easy to write, since the syntax is the same of an R Markdown, immediate to render and, as described later also easy to deploy.

# Deployment solutions

Tutorials rendered via ``learnr`` can be published all of the same ways that Shiny applications can, including running locally on an end-user’s machine or running on a Shiny Server or hosting service like shinyapps.io.

## R package

The easiest, safest, probably most portable and or sure most comprehensive solution is to pack the tutorials within an R package.

This amounts to build an R package, which is also relatively easy and straightforward, and make it available to third parties users via Gitlab (either with or without CI services).

Once the package is built and hosted in Gitlab, the user can download it and install it via the command

``` r
devtools::install_git("path_to_package")
```

and, upon installation of package ``learnr``, run the interactive tutorial via the command

``` r
library(package_name)
learnr::run_tutorial("tutorial_name", package = "dsRtutorials")
```

`` IMPORTANT!``
The reason why the R package would be, in the author's opinion, solution n.1 to deploy a tutorial is that a package is a comprehensive way to deploy, more in general, R code, meaning also scripts, functions, etc..
Consequently, if a group of professional data scientist would like to provide their clients with a set of professional products, i.e. software, reporting templates, tutorials, all bundled together into one portable object, an R package is for sure a very convenient solution. Furthermore, it should be considered that a package is an advantageous solution also for what concerns data security. Infact, the tutorials will be run directly on the users machine, and the data used for the tutorial can be taken directly from the user's machine / network, without any need for private data to be stored online. Alternatively, one can insert in the package preprocessed datasets (synthetic or real), that will also be immediately available to the user inside the tutorial.

In case one does not have the time or knowledge to write an R package, there are some other methods, which are though a little less recommandable from the data security point of view.

## RStudio Connect and shinyapps.io

One can also deploy tutorials on a server (as any other Shiny application). There are several ways to do this, two of which are:

    Publish to the shinyapps.io cloud service.

    Publish to an RStudio Connect server.


## Shiny Server

Another way of publishing your tutorial is to serve it as a .Rmd document in a hosted directory of Shiny Server.

Shiny Server fully supports interactive R Markdown documents. To take advantage of this capability, one will need to make sure that rmarkdown is installed and available for all users.

